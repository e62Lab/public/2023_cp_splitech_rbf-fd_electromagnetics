### Computational Electromagnetics with the RBF-FD Method

This repository has all the code used to generate the above [SpliTech 2023](https://splitech.org/home) conference paper.

#### Abstract

One of the most popular methods employed in computational electromagnetics is the Finite Difference Time Domain (FDTD) method. We generalise it to a meshless setting using the Radial Basis Function generated Finite Difference (RBF-FD) method and investigate its properties on a simple test problem.

#### Authors
- Andrej Kolar-Požun
- Gregor Kosec

#### Repository structure overview
- **Scripts** C++ and Python codes used to generate the data and the figures.
- **Figures** All the figures used in the paper.
- **Paper**: All the LaTeX files used to generate the paper.
- *NeumannStability.pdf* is just a collection of notes with further explanation on how the stability analysis is performed.



For any questions regarding the paper, we are available at andrej.pozun@ijs.si