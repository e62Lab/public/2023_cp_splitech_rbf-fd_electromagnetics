import numpy as np
import matplotlib.pyplot as plt
import math


"""This scripts implements the FDTD on the grid, to be used for comparison with our method"""

def ezinc_harmonic(m, q, Sc, Np):
    """Harmonic source

    Parameters
    ----------
    m : double
        Location
    q : double
        time.
    Sc : double
        Courant #.
    Np : Double
        Point per wavelength.
    """
    
    #q = q-3 #start at time 3
    y = np.sin(2*np.pi*(Sc*(q+0.5)-m)/Np)
    return y


Np = 30

max_m = 601 #rows
max_n = 601 #cols
max_t = 300
#max_t = 600 #halved dt for comparison

src_x = math.floor(max_m/2)
src_y = math.floor(max_n/2)

cdtds = 1/np.sqrt(2) #courant num
imp_0 = 120*np.pi #free space impedance sqrt(mu0/eps0)



#TMz polarization
Hx = np.zeros((max_t,max_m,max_n-1))
Hy = np.zeros((max_t,max_m-1,max_n))


Ez = np.zeros((max_t,max_m,max_n)) #t x y


for q in range(2,max_t):
    aq = q+1
    Hx[q] = Hx[q-1] - cdtds/imp_0*(Ez[q-1,:,1:] - Ez[q-1,:,:-1])
    Hy[q] = Hy[q-1] + cdtds/imp_0*(Ez[q-1,1:,:] - Ez[q-1,:-1,:])
    
    Ez[q,1:-1,1:-1] = Ez[q-1,1:-1,1:-1] + cdtds*imp_0 * ((Hy[q,1:,1:-1] - Hy[q,:-1,1:-1]) - (Hx[q,1:-1,1:] - Hx[q,1:-1,:-1]))
                            
    Ez[q][src_x, src_y] = ezinc_harmonic(0,aq,cdtds,Np)

np.save("Data/30HarmonicEz",Ez)
