#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//THIS SCRIPTS GENERATES THE TIME EVOLVED EZ FIELDS FOR OUR PROBLEM

double ezinc_harmonic(double m, double q, double Sc, double Np){
    q = q-3;
    return sin(2*PI*(Sc*(q+0.5)-m)/Np);
}


void solve(int n){
    double Np = 30;
    double m = 601;
    double max_t = 300;
    double src = static_cast<int>(m/2); //source coord
    double cdtds = 1/sqrt(2); //courant c*dt/dx
    double mu0 = 4*PI*1E-7;
    double eps0 = 8.85* 1E-12;
    double c0 = 3*1E8; //c^2 = 1/mueps
    double imp0 = 120*PI;
    double dx = 1; //601x601 grid
    double dt = cdtds*dx/c0;
    double eps = 1e-10; //for floating comparison

    std::ostringstream strs;
    strs << "Data/MeshlessYeen" << n << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);

    BoxShape<Vec2d> domain({0,0},{m-1,m-1}); //mxm pts

    auto discretization = domain.discretizeWithStep(dx);


    FindClosest find_support(n);
    discretization.findSupport(find_support);
    int domain_size = discretization.size();

    Monomials<Vec2d> mon(2);
    RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
    auto storage = discretization.computeShapes(approx);

    auto op = storage.explicitOperators();
    VectorXd Hx = VectorXd(domain_size); Hx.setZero();
    VectorXd Hy = VectorXd(domain_size); Hy.setZero();
    VectorXd Ez = VectorXd(domain_size); Ez.setZero();
    int sourceInd = -1; //index of the hard source node
    for (int i : discretization.interior()){
        double x = discretization.pos(i,0);
        double y = discretization.pos(i,1);
        if (abs(x-src) < eps && abs(y-src) < eps) sourceInd = i;
    }

    hdf_out.writeDouble2DArray("pos", discretization.positions());
    for (int tt=2; tt < max_t; ++tt){

        for (int i : discretization.interior()){
            Hx(i) = Hx(i) - dt/mu0 * op.d1(Ez,1,i);
            Hy(i) = Hy(i) + dt/mu0 * op.d1(Ez,0,i);
        }

        for (int i : discretization.interior()){
            Ez(i) = Ez(i) + dt/eps0*(op.d1(Hy,0,i) - op.d1(Hx,1,i));
        }

        Ez(sourceInd) = ezinc_harmonic(0,(tt+1),cdtds,Np);

        if (true){
            hdf_out.openGroup("/step" + std::to_string(tt));
            std::cout << "Time step " << tt << std::endl;
            hdf_out.writeDoubleArray("Ez", Ez);

        }
    }
    hdf_out.closeFile();
}

int main() {
    solve(6);
    return 0;
}