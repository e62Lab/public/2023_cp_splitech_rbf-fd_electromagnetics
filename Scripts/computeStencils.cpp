#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <iostream>

using namespace mm;
using namespace Eigen;

//RETURN THE STENCILS AND WEIGHTS FOR OUR SETUP FOR THE PURPOSE OF THE STABILITY ANALYSIS

void solve(int n){
    double m = 601;
    double src = static_cast<int>(m/2); //source coord
    double dx = 1; //601x601 grid

    std::ostringstream strs;
    strs << "Data/ShapesSupportsn" << n << ".h5";
    HDF hdf_out(strs.str(), HDF::DESTROY);


    BoxShape<Vec2d> domain({0,0},{m-1,m-1}); //mxm pts
    auto discretization = domain.discretizeWithStep(dx);

    


    FindClosest find_support(n);
    discretization.findSupport(find_support);
    int domain_size = discretization.size();

    Monomials<Vec2d> mon(2);
    RBFFD<Polyharmonic<double,3>,Vec2d,ScaleToFarthest> approx({},mon);
    auto storage = discretization.computeShapes(approx);

    hdf_out.writeDouble2DArray("pos", discretization.positions());

    for (int i : discretization.interior()){
        hdf_out.openGroup("/position" + std::to_string(i));
        hdf_out.writeDoubleArray("sup",discretization.support(i));
        hdf_out.writeDoubleArray("dx",storage.d1(0,i));
        hdf_out.writeDoubleArray("dy",storage.d1(1,i));
    }
    hdf_out.closeFile();
}

int main() {
    for (int n=6; n<=15; ++n) solve(n);
    return 0;
}