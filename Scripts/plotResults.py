import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

plt.rcParams.update({
    "mathtext.fontset": "cm",
    "font.family": "STIXGeneral",
    "text.usetex": False,
    "axes.formatter.use_mathtext": True,
    "xtick.labelsize": 12,
    "ytick.labelsize": 12,
    "axes.labelsize": 14,
    "axes.xmargin" : 0
})

"""This script plots the Yee vs RBF-FD comparison plots (contours)"""

def adjustTickFont(*axes, labelsize=20):
    """Helper function to resize axis ticks"""
    
    for ax in axes:
        ax.tick_params(axis='both', which='major', labelsize=labelsize)
        ax.tick_params(axis='both', which='minor', labelsize=labelsize)



def plotComparison():
    
    Ez = np.load("Data/30HarmonicEz.npy")


    f = h5.File("Data/MeshlessYeen6.h5","r")
    x,y = f['pos'][:]

    Z = np.zeros((300,601,601))
    

    for i in range(2,300):
        steps = f[f'step{i}']['Ez'][:]
        for j in range(len(steps)):
            xx = int(x[j])
            yy = int(y[j])
            Z[i][xx][yy] = steps[j]
    f.close()
    f = h5.File("Data/MeshlessYeeFinern6.h5","r")
    x,y = f['pos'][:]

    Z2 = np.zeros((300,601,601))
    

    for i in range(2,300):
        steps = f[f'step{2*i-2}']['Ez'][:] #2 4
        for j in range(len(steps)):
            xx = int(x[j])
            yy = int(y[j])
            if abs(x[j]-xx)>1e-5 or abs(y[j]-yy) > 1e-5:
                continue
            Z2[i][xx][yy] = steps[j]
    f.close()
    Zs = [Ez,Z,Z2]
    levels = np.linspace(-1,1,50)
    one = np.arange(1,602)
    X, Y = np.meshgrid(one,one, indexing='ij')

    fig, axi = plt.subplots(3,3,sharex=True,sharey=True, figsize=(8,8))


    axi[0][0].set_title(r"Yee, $\Delta s = 1$", fontsize=17)
    axi[0][1].set_title(r"RBF-FD, $\Delta s = 1$", fontsize=17)
    axi[0][2].set_title(r"RBF-FD, $\Delta s = 0.5$", fontsize=17)
    cmap = "seismic"
    for i in range(1,4):
        for j in range(3):
            sc = axi[i-1][j].pcolormesh(X,Y,Zs[j][100*i-1], cmap=cmap, norm=mpl.colors.Normalize(-1,1))
            axi[i-1][j].set_aspect(1)
            if i==1:
                axins = axi[i-1][j].inset_axes([0.6,0.6,0.3,0.3])
                axins.pcolormesh(X,Y,Zs[j][100*i-1], cmap=cmap, norm=mpl.colors.Normalize(-1,1))
                axins.set_xlim(296,306)
                axins.set_ylim(296,306)
                axins.set_xticks([])
                axins.set_yticks([])
                #axins.axis('off')
                axi[i-1][j].indicate_inset_zoom(axins,edgecolor='black')
        axi[i-1][0].set_ylabel(f"$n={100*i}$", fontsize=17)


    axi[0][0].set_xlim(50,550)
    axi[0][0].set_ylim(50,550)
    

    for i in range(3):
        for j in range(3):
            adjustTickFont(axi[i][j], labelsize=17)
    fig.subplots_adjust(bottom=0.1, top=0.9, left=0.1, right=0.8)
    cb_ax = fig.add_axes([0.83, 0.1, 0.02, 0.8])
    cb1  = mpl.colorbar.ColorbarBase(cb_ax,cmap=cmap,norm=mpl.colors.Normalize(-1,1),orientation='vertical', label=r'$E_z$')
    adjustTickFont(cb_ax,labelsize=17)
    cb_ax.tick_params(labelsize=17)

    plt.savefig("../Figures/contours.png",dpi=300, transparent = False, bbox_inches='tight')

plotComparison()