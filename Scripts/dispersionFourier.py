import matplotlib.pyplot as plt
import numpy as np
import h5py as h5
import matplotlib.cm
import matplotlib as mpl

plt.rcParams.update({
    "mathtext.fontset": "cm",
    "font.family": "STIXGeneral",
    "text.usetex": False,
    "axes.formatter.use_mathtext": True,
    "xtick.labelsize": 12,
    "ytick.labelsize": 12,
    "axes.labelsize": 14,
    "axes.xmargin" : 0
})

"""This script plots the contours for bigger stencils + spectra"""

def adjustTickFont(*axes, labelsize=20):
    """Helper function to adjust axis tick size"""
    
    for ax in axes:
        ax.tick_params(axis='both', which='major', labelsize=labelsize)
        ax.tick_params(axis='both', which='minor', labelsize=labelsize)

timemax = 299

Zs = []
spaceRange = np.arange(601)
timeRange = np.arange(timemax)



for n in [6,9,13,25]:
    filename= f"Data/MeshlessYeeFinern{n}.h5"
    f = h5.File(filename)
    x,y = f['pos'][:]

    Z = np.zeros((timemax,601,601))
    for t in range(2,timemax+2):
        res = f[f"step{2*t-1}"]["Ez"][:]
        for j in range(len(x)):
            xx = int(x[j])
            yy = int(y[j])
            if abs(x[j]-xx)>1e-5 or abs(y[j]-yy) > 1e-5:
                continue
            Z[t-2][xx][yy] = res[j]
            
    f.close()
    Zs.append(Z)




fig= plt.figure(constrained_layout=True, figsize=(5.5,8))
gs = fig.add_gridspec(5,2,height_ratios=[0.05,1,1,1,1], width_ratios=[1,2])
ax = [[fig.add_subplot(gs[i,0]),fig.add_subplot(gs[i,1])] for i in range(1,5)]

one = np.arange(1,602)
X, Y = np.meshgrid(one,one, indexing='ij')
labels = ["$ss=6$","$ss=9$","$ss=13$","$ss=25$"]
for i in range(4):
    sc = ax[i][0].pcolormesh(X,Y,Zs[i][150], cmap="seismic", norm=mpl.colors.Normalize(-1,1))
    #fig.colorbar(sc,ax=ax[i][0])
    ax[i][0].set_aspect(1)
    ax[i][0].set_xticks([])
    ax[i][0].set_yticks([])
    ax[i][0].set_ylabel(labels[i])

cbax = fig.add_subplot(gs[0,0])
cb = mpl.colorbar.ColorbarBase(cbax, orientation='horizontal',cmap="seismic", norm=mpl.colors.Normalize(-1,1))
#cbax.tick_params(labelsize=10)

hannTime = np.sin(np.pi*np.arange(timemax)/(timemax-1))**2
hannTime = np.tile(hannTime,(601,1))
hannTime = np.transpose(hannTime)

for i in range(4):
    Zs[i] = Zs[i][:,:,300]
    Zs[i] = Zs[i]*hannTime

spacemax = 601
spacemax = int(spacemax/2)+1
timemax = int(timemax/2)+1
spaceRange = np.arange(0,spacemax)
timeRange = np.arange(0,timemax)

Fts = []
for i in range(4):
    ft = np.fft.fft2(np.transpose(Zs[i]))
    ft = np.abs(ft[:spacemax,:timemax])
    Fts.append(ft)  

maxi = max([np.amax(i) for i in Fts])
Fts = [f/maxi for f in Fts]

X,Y = np.meshgrid(spaceRange, timeRange, indexing='ij')
ax[0][0].set_title("$E_z, n=150$", fontsize=10)
ax[0][1].set_title("$|E_z(\omega, k)|$, normalised.", fontsize=10)
kxs = np.array([i for i in range(60)])
for i in range(4):
    sc = ax[i][1].pcolormesh(X,Y,Fts[i], cmap="winter", norm=mpl.colors.Normalize(0,1))
    ax[i][1].set_xlim(0,50)
    ax[i][1].set_ylim(0,20)
    ax[i][1].set_ylabel(r"$\omega$", fontsize=10)
    ax[i][1].plot(kxs,np.sqrt(2)/2*timemax/spacemax*kxs,color="k", label=r"Speed of light")
    adjustTickFont(ax[i][1],labelsize=10)
ax[0][1].legend(loc="upper left", fontsize=10)
ax[3][1].set_xlabel(r"$k$", fontsize=10)
cbax2 = fig.add_subplot(gs[0,1])
cb = mpl.colorbar.ColorbarBase(cbax2, orientation='horizontal',cmap="winter", norm=mpl.colors.Normalize(0,1))
#cbax2.tick_params(labelsize=10
adjustTickFont(ax[i][1],labelsize=10)
adjustTickFont(cbax,cbax2,labelsize=10)
plt.savefig("../Figures/dispersion.png",dpi=300, transparent = False, bbox_inches='tight')
