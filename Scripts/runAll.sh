#!/bin/bash

if [ ! -d "./Data" ]
then
echo Making the Data directory...
mkdir Data
fi


echo Running all the scripts...
for filename in MeshlessYee.cpp MeshlessYeeFiner.cpp computeStencils.cpp
do
echo Compiling $filename...
filenameBase=$(basename $filename .cpp)
g++ -o $filenameBase "${filenameBase}.cpp" -I ../../medusa/include -I /usr/include/hdf5/serial -L ../../medusa/bin/ -L /usr/lib/x86_64-linux-gnu/hdf5/serial/ -l medusa_standalone -l hdf5 -Wall
echo Running it...
./$filenameBase
rm $filenameBase
done

for filename in Yee.py plotResults.py stability.py dispersionFourier.py
do
echo Running the Python script $filename...
python3 $filename
done
