import numpy as np
import h5py as h5
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from scipy.optimize import newton


"""This paper has the Von Neumann stabiilty analysis and plots the results"""

imp0 = 120*np.pi

plt.rcParams.update({
    "mathtext.fontset": "cm",
    "font.family": "STIXGeneral",
    "text.usetex": False,
    "axes.formatter.use_mathtext": True,
    "xtick.labelsize": 12,
    "ytick.labelsize": 12,
    "axes.labelsize": 14,
    "axes.xmargin" : 0
})

def adjustTickFont(*axes, labelsize=20):
    """Helper function to change label size of ticks"""
    
    for ax in axes:
        ax.tick_params(axis='both', which='major', labelsize=labelsize)
        ax.tick_params(axis='both', which='minor', labelsize=labelsize)


#ALL THESE ASSUME INTERVINED LEAPFROG FOR TIME EVOLUTION

def nonSymmetricStencil(derx,dery,Sc):
    """Find spectral radius for a given Courant number Sc and complex numbers derx/dery (wx,wy in latex notes)"""
    
    c1 = Sc/imp0
    c2 = Sc*imp0
    term1 = 2+c1*c2*(derx*derx+dery*dery)
    term2 = c1*c2*(derx*derx+dery*dery)*(4+c1*c2*(derx*derx+dery*dery))
    lamb1 = 0.5*(term1-np.sqrt(term2))
    lamb2 = 0.5*(term1+np.sqrt(term2))
    return max(np.abs(lamb1),np.abs(lamb2))


def derivative(kx,ky,ws,distsx, distsy):
    """Returns derivative approx for given weights(shapes)/distancess"""
    rez = 0
    for w,distx, disty in zip(ws,distsx,distsy):
        rez += w*np.exp(1j*(kx*distx+ky*disty))
    return rez



def printAmplifications(wx,wy,distsx, distsy):
    """Prints spectral radius for several diff courant numbers"""
    
    Scs = np.linspace(0,1,100)
    argumenti = np.linspace(0,2*np.pi,1000)
    for Sc in Scs:
        vrednosti = [nonSymmetricStencil(derivative(kx,ky,wx,distsx,distsy), derivative(kx,ky,wy,distsx,distsy),Sc) for kx in argumenti for ky in argumenti]
        vrednost = max(vrednosti)
        print(f"Sc: {Sc}, vred: {vrednost}")
   
        
def returnBound(wx,wy,distsx, distsy):
    """Returns boundary Sc at which we start to get unstabilities"""
    
    Scs = np.linspace(0,2,1000)
    argumenti = np.linspace(0,2*np.pi,100)
    for i,Sc in enumerate(Scs):
        vrednosti = [nonSymmetricStencil(derivative(kx,ky,wx,distsx,distsy), derivative(kx,ky,wy,distsx,distsy),Sc) for kx in argumenti for ky in argumenti]
        vrednost = max(vrednosti)
        if vrednost > 1+1e-7:
            if i==0:
                return 0
            else:
                return Scs[i-1]
    return Scs[-1]
 
    
def findBoundaryForDomain(filename):
    """For now, pick an arbitrary node (5000) 
    and calculate the stability criterion for its stencil"""
    
    f = h5.File(filename,"r")
    x, y = f['pos'][:]
    node = 5000
    support = f[f'position{node}']['sup'][:]
    wx = f[f'position{node}']['dx'][:]
    wy = f[f'position{node}']['dy'][:]
    f.close()
    distsx = [x[int(p)]-x[node] for p in support]
    distsy = [y[int(p)]-y[node] for p in support]
    #dists = [x[int(p)]-x[node]+y[int(p)]-y[node] for p in support]
    #printAmplifications(wx, wy, dists)
    bound = returnBound(wx,wy,distsx, distsy)
    return bound
#a = findBoundaryForDomain("ShapesSupportsn9.h5")





#==================== Here plotting stencils+weights

def plotStencilsWeights():
    ns = list(range(6,15))
    xs = []
    ys = []
    wxs = []
    wys = []
    Scs = []
    for n in ns:
        path = f"Data/ShapesSupportsn{n}.h5"
        Sc = round(findBoundaryForDomain(path),2)
        f = h5.File(path,"r")
        pos = f['pos'][:]
        support = f['position10000']['sup'][:] #arbitrary chosen position 10000 (not near boundary)
        x = [pos[0][int(p)] for p in support]
        y = [pos[1][int(p)] for p in support]
        wx = f['position10000']['dx'][:]
        wy = f['position10000']['dy'][:]
        xs.append(x)
        ys.append(y)
        wxs.append(wx)
        wys.append(wy)
        Scs.append(Sc)
    
    cmap = cm.get_cmap("coolwarm")   
    fig, ax = plt.subplots(3,3,figsize=(8,8), sharex=True, sharey=True)
    axi = [a for b in ax for a in b]
    minw = min([min(i) for i in wys])
    maxw = max([max(i) for i in wys])
    #norm = mpl.colors.SymLogNorm(1e-7, vmin=minw, vmax=maxw) 
    norm = mpl.colors.Normalize(vmin=minw,vmax=maxw)

    
    for i in range(9):
        axi[i].set_title(f"$ss={ns[i]}, S_c={Scs[i]}$", fontsize=16)
        axi[i].scatter(xs[i][1:],ys[i][1:],c=wys[i][1:], edgecolors='black',cmap=cmap,norm=norm)
        axi[i].scatter(xs[i][0],ys[i][0],c=wys[i][0],cmap=cmap, norm=norm, marker='s',edgecolors='black')
        axi[i].set_xticks([])
        axi[i].set_yticks([])
        axi[i].set_xlim(xs[i][0]-2.5,xs[i][0]+2.5)
        axi[i].set_ylim(ys[i][0]-2.5,ys[i][0]+2.5)        
    fig.subplots_adjust(bottom=0.1, top=0.9, left=0.1, right=0.8)
    cb_ax = fig.add_axes([0.83, 0.1, 0.02, 0.8])
    cb1  = mpl.colorbar.ColorbarBase(cb_ax,cmap=cmap,norm=norm,orientation='vertical', label=r'$w_y$')
    adjustTickFont(cb_ax,labelsize=16)
    cb_ax.tick_params(labelsize=16)

    plt.savefig("../Figures/stencilsStability.png",dpi=300, transparent = False, bbox_inches='tight')

plotStencilsWeights()


        
        
        

